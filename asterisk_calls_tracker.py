import re
import time, datetime
import logging
import asterisk.manager
import configuration as conf
from calltouch_api import CallTouch
from tagfile_parser import check_tagfile

manager = asterisk.manager.Manager()
manager.connect(conf.ami_host)
manager.login(conf.ami_login, conf.ami_pass)


logging.basicConfig(format='%(levelname)-8s [%(asctime)s] %(message)s',
                    level=logging.INFO,
                    filename='/var/log/asterisk/calltouch.log')

idlist = {} # Список пойманных звонков в виде объектов
direct_call_idlist = {} # Список пойманных прямых звонков в виде объектов
api_obj = {} # Список объектов API (разные учетки)
tag_file_path = conf.tag_file # Путь до файла с тегами
hold_call = {} # Список звонков которые поставлены на холд и возможно будут переведены

avaliable_tags = check_tagfile(tag_file_path)
# Отдельный объект для API, который используется для отправки сообщений о входящем звонке не через CAllTouch
inboundAPI = CallTouch(conf.accounts_inbound['API_KEY'], conf.accounts_inbound['SITE_ID'], conf.accounts_inbound['API_HOST'])

for a in conf.accounts:
    class_obj = CallTouch(a['API_KEY'], a['SITE_ID'], a['API_HOST'])
    i = class_obj.site_id
    api_obj[i] = {'API': class_obj}


def normalize_number(raw_number):
    # Удаляем буквы и символы, оставляем только цифры
    if raw_number.isdigit() is True:
        return raw_number
    normalize_number = re.sub("\D", "", raw_number)
    return normalize_number


def get_id(number, start_period=0):
    if start_period == 0:
        start_period = datetime.datetime.now()
    logging.debug(f'Получаем ID из Calltouch по номеру {number}')
    for i in api_obj:
        time.sleep(2)
        call_id = api_obj[i]['API'].get_id(number, start_period)
        if call_id is not False:
            return call_id, i
    return False, 0


def set_tag(call_id, tag, api_id):
    api_obj[api_id]['API'].set_tag(call_id, tag)


def get_tag(raw_number):
    logging.debug(f'Получаем тег по номеру {raw_number}')
    number = normalize_number(raw_number)
    if number in avaliable_tags:
        tag = avaliable_tags[number]
    elif (number[0] == '0' or number[0] == '9') and len(number) > 6:
        tag = 'MOB_' + number[1:]
    elif (number[0] == '8' or number[0] == '7') and len(number) > 10:
        tag = 'MOB_' + number
    else:
        tag = number
    logging.debug(f'Полученный тег - {tag}')
    return tag


class CalltouchCall:

    def __init__(self, callerid, calltouchid, apiid, uid):
        self.uid = uid
        self.callerid = callerid
        self.calltouchid = calltouchid
        self.apiid = apiid
        self.tag = 'None'
        self.hold = False
        self.answernubmer = ''
        self.transfernumber = ''

    def print_data(self):
        print('CalltouchCall')
        print(self.uid,
              self.callerid,
              self.calltouchid,
              self.apiid,
              self.tag,
              self.answernubmer,
              self.transfernumber)


class DirectCall:

    def __init__(self, callerid, uid, startcall):
        self.uid = uid
        self.callerid = callerid
        self.calltouchid = ''
        self.startcall = startcall
        self.duration = '0'
        self.waitingtime = '0'
        self.status = ''
        self.hold = False
        self.tag = 'None'
        self.answernubmer = ''
        self.transfernumber = ''

    def print_data(self):
        print('DirectCall')
        print(self.uid,
              self.callerid,
              self.calltouchid,
              self.startcall,
              self.duration,
              self.waitingtime,
              self.status,
              self.tag,
              self.answernubmer,
              self.transfernumber)


def catch_call(event, manager):
    if (event.name == 'Newchannel' and len(event.headers['CallerIDNum']) > 6 and
            event.headers['Context'] == 'from-trunk' and len(event.headers['Exten']) > 8):
        logging.info(f"[{event.headers['Uniqueid']}] Новый входящий звонок {event.headers['CallerIDNum']}")
        call_id, api_id = get_id(event.headers['CallerIDNum'])
        if call_id == False and event.headers['Exten'] == '74959331055':
            # Добавляем прямой звонок в отдельный список
            direct_call_idlist[event.headers['Uniqueid']] = DirectCall(event.headers['CallerIDNum'],
                                                                       event.headers['Uniqueid'],
                                                                       datetime.datetime.now())
            logging.debug(f'Новый прямой звонок с номер {event.headers["CallerIDNum"]}')
        elif call_id is not False:
            logging.info(f'[{event.headers["Uniqueid"]}] Входящий номер '
                          f'{event.headers["CallerIDNum"]} в Calltouch c ID {call_id}')

            idlist[event.headers['Uniqueid']] = CalltouchCall(event.headers['CallerIDNum'],
                                                              call_id, api_id, event.headers['Uniqueid'])

        else:
            logging.info(f'[{event.headers["Uniqueid"]}] Звонка нет в Calltouch')


def catch_agentconnect(event, manager):

    if event.headers['Linkedid'] in direct_call_idlist:
        # Оператор ответил на звонок с прямого номера
        call = direct_call_idlist[event.headers['Linkedid']]
        call.waitingtime = event.headers['HoldTime']
        call.answernubmer = event.headers['ConnectedLineNum']
        logging.info(f'[{call.uid}] Оператор {call.answernubmer} ответил на прямой звонок {call.callerid}')
    elif event.headers['Linkedid'] in idlist:
        # Оператор ответил на звонок поступивший от Calltouch
        call = idlist[event.headers['Linkedid']]
        call.answernubmer = event.headers['ConnectedLineNum']
        logging.info(f'[{call.uid}] Оператор {call.answernubmer} ответил на звонок {call.callerid}')


def catch_hold(event, manager):

    if event.headers['Linkedid'] in direct_call_idlist:
        call = direct_call_idlist[event.headers['Linkedid']]
        call.hold = True
        hold_call[call.answernubmer] = call.uid
        logging.info(f'[{call.uid}] Оператор поставил звонок {call.callerid} на HOLD')
    elif event.headers['Linkedid'] in idlist:
        call = idlist[event.headers['Linkedid']]
        call.hold = True
        hold_call[call.answernubmer] = call.uid
        logging.info(f'[{call.uid}] Оператор поставил звонок {call.callerid} на HOLD')


def catch_unhold(event, manager):
    if event.headers['Linkedid'] in direct_call_idlist:
        call = direct_call_idlist[event.headers['Linkedid']]
        call.hold = False
        del hold_call[event.headers['CallerIDNum']]
        logging.info(f'[{call.uid}] Оператор снял звонок {call.callerid} с HOLD')
    elif event.headers['Linkedid'] in idlist:
        call = idlist[event.headers['Linkedid']]
        call.hold = False
        del hold_call[event.headers['CallerIDNum']]
        logging.info(f'[{call.uid}] Оператор снял звонок {call.callerid} с HOLD')


def dialbegin(event, manager):

    if event.headers['CallerIDNum'] in hold_call:
        logging.debug(event.headers)
        uid = hold_call[event.headers['CallerIDNum']]
        if uid in idlist:
            call = idlist[uid]
            if call.hold is True:
                call.transfernumber = event.headers['DestCallerIDNum']
                logging.info(f'[{call.uid}] Оператор пытается перевести звонок {call.callerid} на номер {call.transfernumber}')
        elif uid in direct_call_idlist:
            call = direct_call_idlist[uid]
            if call.hold is True:
                call.transfernumber = event.headers['DestCallerIDNum']
                logging.info(f'[{call.uid}] Оператор пытается перевести звонок {call.callerid} на номер {call.transfernumber}')


def catch_transfer(event, manager):

    if event.headers['OrigTransfererLinkedid'] in idlist:
        call = idlist[event.headers['OrigTransfererLinkedid']]
    elif event.headers['OrigTransfererLinkedid'] in direct_call_idlist:
        call = direct_call_idlist[event.headers['OrigTransfererLinkedid']]
    else:
        return False
    logging.debug(f'Поймано событие catch_transfer с данными {event.headers}')
    try:
        transfer_number = event.headers['TransferTargetCallerIDNum']
    except KeyError:
        logging.debug('Не удалось найти ключ TransferTargetCallerIDNum, пробуем найти SecondTransfererConnectedLineNum')
        transfer_number = event.headers['SecondTransfererConnectedLineNum']

    logging.info(f'[{call.uid}] Звонок {call.callerid} был перевден на номер {transfer_number}')
    tag = get_tag(transfer_number)
    call.tag = tag
    logging.info(f'[{call.uid}] Звонок получил тег {tag}')


def dialend(event, manager):

    if event.headers['DestLinkedid'] in direct_call_idlist:
        logging.debug(f"Пойман конец разговора для прямого звонка {event.headers['DestLinkedid']} "
                      f"со статусом {event.headers['DialStatus']}")
        call = direct_call_idlist[event.headers['DestLinkedid']]
        if event.headers["DialStatus"] == 'ANSWER':
            call.status = 'successful'
        elif event.headers["DialStatus"] != 'ANSWER':
            if call.status == 'successful':
                pass
            else:
                call.status = 'unsuccessful'


def hangup_calls(event, manager):

    if event.headers['Uniqueid'] in idlist:
        call = idlist[event.headers['Uniqueid']]
        if call.tag != 'None':
            set_tag(call.calltouchid, call.tag, call.apiid)
            logging.info(f'[{call.uid}] Отправляем информацию о звонке {call.callerid} с тегом {call.tag}')
        else:
            if len(call.transfernumber) > 2:
                tag = get_tag(call.transfernumber)
                tag = tag+'_noanswer'
                set_tag(call.calltouchid, tag, call.apiid)
                logging.info(f'[{call.uid}] Отправляем информацию о звонке {call.callerid} с тегом {tag}')

        del idlist[event.headers['Uniqueid']]
    elif event.headers['Uniqueid'] in direct_call_idlist:
        logging.debug(event.headers)
        call = direct_call_idlist[event.headers['Uniqueid']]
        call_end_time = datetime.datetime.now()
        call.duration = str((call_end_time - call.startcall).total_seconds()).split('.')[0]
        if call.status == 'unsuccessful' or call.status == '':
            call.duration = '0'
        request, uri = inboundAPI.send_call(call.uid, call.callerid, call.startcall, call.duration, call.waitingtime, call.status)
        logging.debug(f'Прямой звонок. Создано URL {uri} {request}')
        time.sleep(2)
        if call.status == 'successful':
            call_id, api_id = get_id(call.callerid, call.startcall)
            logging.debug(f'Получили информацию о номере {call.callerid} : call_id {call_id}, api_id {api_id}')
            if call.tag != 'None':
                set_tag(call_id, call.tag, api_id)
                logging.info(f'[{call.uid}] Отправляем информацию о звонке {call.callerid} с тегом {call.tag}')
            else:
                if len(call.transfernumber) > 2:
                    tag = get_tag(call.transfernumber)
                    tag = tag + '_noanswer'
                    set_tag(call_id, tag, api_id)
                    logging.info(f'[{call.uid}] Отправляем информацию о звонке {call.callerid} с тегом {tag}')
                else:
                    logging.info(f'[{call.uid}] Звонок {call.callerid} никому не переводили')
        del direct_call_idlist[event.headers['Uniqueid']]


manager.register_event('Newchannel', catch_call)
manager.register_event('AttendedTransfer', catch_transfer)
manager.register_event('DialEnd', dialend)
manager.register_event('DialBegin', dialbegin)
manager.register_event('Hangup', hangup_calls)
manager.register_event('Hold', catch_hold)
manager.register_event('Unhold', catch_unhold)
manager.register_event('AgentConnect', catch_agentconnect)

while True:
    ping = {'Action':'Ping'}
    pong = manager.send_action(ping)
    logging.debug(pong)
    time.sleep(50)