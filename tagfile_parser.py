import re, logging


def check_tagfile(filename):
    tag_list = {}
    badlines = []
    tag_regex = r'^\d*:.*$'
    try:
        tagfile = open(filename, 'r')
    except Exception as ex:
        logging.debug(
            f'{ex} '
            f'Ошибка! Не могу открыть файл {filename}. Проверьте правильность расположения и имя файла.'
            f' Продолжаю работу без списка тегов!')
        return tag_list
    else:
        for line in enumerate(tagfile, start=1):
            if line[1][0] == '#':
                badlines.append(line[0])
            else:
                regex_search = re.search(tag_regex, line[1])
                if regex_search:
                    tag_list[line[1].split(':')[0]] = line[1].rstrip().split(':')[1]
                    pass
                else:
                    badlines.append(line[0])
                    logging.debug(f'Проверьте файл {filename}. Ошибка в строке №{line[0]} "{line[1][:-1]}"')

        tagfile.close()
        return tag_list

