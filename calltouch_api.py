import json
import requests
import configuration as conf
from datetime import timedelta, datetime


class CallTouch:

    """

    CallTouch класс для работы с сервисолм CallTouch.

    Функция get_id принимает на вход номер телефона в формате 7ХХХХХХХХХХ и возвращает либо ID звонка, либо
    False. В качестве допольнительного фильтра используется текущая дата и 5 минутный временной промежуток.
    То есть, в сервисе CallTouch проверяется номер только за текущую дату и 5 минутный диапазон за 2 до за проса
    и 3 вперед.

    Функция set_tag отправляет тег в сервис. На вход принимает два аргумента ID и тег, обратно возвращает либо
    Sucsses, либо False.

    """

    def __init__(self, api_key, site_id, api_host):

        self.api_key = api_key
        self.site_id = site_id
        self.api_host = api_host

    def _build_uri(self, api_type, api_command, *args):
        uri = list()
        uri.append(f'{self.api_host}')
        uri.append(f'{self.site_id}/')
        uri.append(f'{api_command}')
        if api_type == 'tag':
            uri.append(f'clientApiId={self.api_key}&')
        elif api_type == 'ats':
            uri.append(f'apikey={self.api_key}&')
        for a in args:
            uri.append(f'{a}&')
        uri = "".join(uri)

        return uri[:-1]

    def _parse_json(self, res):
        json_data = json.loads(res.text)
        return json_data

    def _api_request(self, uri):
        response = requests.get(uri)
        res = self._parse_json(response)
        return res

    def get_id(self, number, start_period):
        api_type = 'tag'
        date = datetime.today().strftime("%d/%m/%Y")
        api_command = 'calls-diary/calls?'
        filter = 'ani.value=' + str(number)
        date_from = 'dateFrom=' + date
        date_to = 'dateTo=' + date
        time_from = 'timeFrom='+(start_period - timedelta(minutes=2)).strftime('%H:%M:%S')
        time_to = 'timeTo=' + (start_period - timedelta(minutes=-3)).strftime('%H:%M:%S')
        uri = self._build_uri(api_type, api_command, date_from, date_to, time_from, time_to, filter)
        try:
            call_id = self._api_request(uri)
            return call_id[0]['callId']
        except:
            return False

    def set_tag(self, call_id, tag):
        api_type = 'tag'
        api_command = 'addtags/?'
        call_id = 'callId='+str(call_id)
        tagnames = 'tagnames='+str(tag)
        uri = self._build_uri(api_type, api_command, call_id, tagnames)
        try:
            res = self._api_request(uri)
            return res['status']
        except:
            return False

    def send_call(self, callid, callerid, call_date, duration, waitingtime, status):
        """

        :param callid: Уникальный индитикфактор. Можно использовать из Asterisk UID
        :param callerid: Номер звонящего. В формате 79XXXXXX
        :param duration: Продолжительность звонка
        :param waitingtime: Время ожидания звока
        :param status: Статус ответа бинарный, возможные варианты "successful \ unsuccessful"
        :return: Возвращает стату Sucsess или False
        """
        if status == '':
            status = 'unsuccessful'
        api_type = 'ats'
        api_command = 'inbound-call?model=telemirfact&'
        phonenumber = f"phonenumber={conf.phone_catch_incalls}"
        destphonenumber = f"destphonenumber={conf.phone_catch_incalls}"
        date = call_date.strftime("%Y-%m-%d")
        time = call_date.strftime('%H:%M:%S')
        date_time = f"date={date}%20{time}"
        callid = f"callid={callid}"
        callerid = f"callerid={callerid}"
        duration = f"duration={duration}"
        waitingtime = f"waitingtime={waitingtime}"
        status = f"status={status}"
        uri = self._build_uri(api_type, api_command, callid, callerid, phonenumber,
                              destphonenumber, date_time, duration, waitingtime, status)
        try:
            res = self._api_request(uri)
            return res, uri
        except:
            return 'Error - request not send'



